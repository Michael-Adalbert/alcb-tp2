#!/bin/sh
module="TP2"
device="sample"
mode="664"
# Appel de insmod avec les arguments passes
/sbin/insmod ./$module.ko $* || exit 1
# Supprime l’eventuelle occurrence existante du noeud
rm -f /dev/${device}
major=$(awk "\\$2==\"$module\" {print \\$1}" /proc/devices)
mknod /dev/${device} c $major 0
# Modifie les permissions et change le groupe ("staff" ou "wheel" selon les distrib.)
group="staff"
grep '^staff:' /etc/group > /dev/null || group="wheel"
chgrp $group /dev/${device}
chmod $mode /dev/${device} 