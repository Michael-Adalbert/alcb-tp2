#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/types.h>
#include <linux/uaccess.h>
#include <linux/fs.h>

#define LICENCE "GPL"
#define AUTEUR "Michael Adalbert michael.adalbert@univ-tlse3.fr"
#define DESCRIPTION "Exemple de module Master CAMSI"
#define DEVICE "A_Device"

#define TAILLE_BUFFER 8

#define MIN(a, b) ((a) < (b)) ? (a) : (b)

//----------------------------------------------------------------

//Struct

typedef struct {
    char *data;
    size_t size_data;
} data_t;

typedef struct {
    data_t buffer_content[TAILLE_BUFFER];
    int buffer_size;
    int current_read_idx;
    int current_write_idx;
} buffer_t ;

//Global var
int flag_init = 0;

static buffer_t *buff;

//----------------------------------------------------------------

#define WRITE_LENGTH 8

static ssize_t read_in_buffer(struct file *f,char *buf, size_t s, loff_t *t){
    data_t *d;
    int size_to_copy; 
    //-----------------------------------
    printk(KERN_ALERT "Read called!\n");
    //----------------------------------
    d = &buff->buffer_content[buff->current_read_idx];
    size_to_copy = MIN(d->size_data, WRITE_LENGTH);
    if(d->size_data != 0){
        if(copy_to_user( buf, d->data, size_to_copy) == 0 ){
            d->size_data = 0;
            kfree(d->data);
        }else{
            return -EFAULT;
        }
    }
    return size_to_copy; 
}

static ssize_t write_in_buffer(struct file *f,const char *buf, size_t s, loff_t *t){
    data_t *d;
    //-----------------------------------
    printk(KERN_ALERT "Write called !\n");
    //-----------------------------------
    d = &buff->buffer_content[buff->current_write_idx];
    if(d->size_data != 0){
        kfree(d->data);
    }
    d->data = (char*)kmalloc(MIN(WRITE_LENGTH,s) * sizeof(char),GFP_KERNEL);
    d->size_data = s - copy_from_user(d->data,buf,MIN(WRITE_LENGTH,s));
    return MIN(WRITE_LENGTH,s);
}



//----------------------------------------------------------------

static ssize_t my_read(struct file *f,char *buf, size_t s, loff_t *t){
    ssize_t size_readed;
    size_readed = read_in_buffer( f, buf, s, t);
    printk(KERN_ALERT "INFO : Number of byte read %ld B\n",size_readed);
    if(size_readed > 0){
        buff->current_read_idx = ( buff->current_read_idx + 1 ) % buff->buffer_size;
        return size_readed;
    }
    return 0;
}

static ssize_t my_write(struct file *f,const char *buf, size_t s, loff_t *t){
    ssize_t size_writed;
    if(s > 0){
        size_writed = write_in_buffer( f, buf, s, t);
        printk(KERN_ALERT "INFO Number of byte write %ld B\n",size_writed);
        buff->current_write_idx = ( buff->current_write_idx + 1 ) % buff->buffer_size;
        return size_writed;
    }
    return 0;
}

static int my_open(struct inode *in, struct file *f){
    int i;
    printk(KERN_ALERT "Open called");
    if(flag_init == 0){
        printk(KERN_ALERT "Initialisation buffer \n");
        //Allocation de la structure du buffer
        buff = kmalloc(sizeof(buffer_t),GFP_KERNEL);
        if(!buff){
            printk(KERN_ALERT ">>> ERROR Allocation memoire du buffer\n");
            return 0;
        }

        //initialisation du buffer
        buff->buffer_size = TAILLE_BUFFER;
        buff->current_read_idx = 0;
        buff->current_write_idx = 0;
        for( i = 0 ; i < TAILLE_BUFFER ; i = i + 1){
            buff->buffer_content[i].size_data = 0;
        }
        flag_init = flag_init + 1;
    }
    return 0;
}
static int my_release(struct inode *in, struct file *f){
    printk(KERN_ALERT "Close called \n");
    buff->current_read_idx = 0;
    buff->current_write_idx = 0;
    return 0;
}

//----------------------------------------------------------------

static dev_t dev;

static struct cdev *my_cdev;

static struct file_operations my_fops = {
    .owner = THIS_MODULE,
    .read = my_read,
    .write = my_write,
    .open = my_open,
    .release = my_release,
};

static struct class *cl;

//----------------------------------------------------------------------

static int buff_init(void){
    // intiailisation du pilote
    if( alloc_chrdev_region(&dev,0,1,"sample") == -1 )
    {
        printk(KERN_ALERT">>> ERROR alloc_chrdev_region\n");
        return -EINVAL;
    }
    printk(KERN_ALERT "Init allocated (major, minor)=(%d,%d)\n",MAJOR(dev),MINOR(dev));
    printk(KERN_ALERT "Driver is starting\n");

    my_cdev = cdev_alloc();
    my_cdev->ops = &my_fops;
    my_cdev->owner = THIS_MODULE;

    cl = class_create( THIS_MODULE, "chardev" );

    if ( cl == NULL )
    {
        printk( KERN_ALERT ">>> ERROR : Class creation failed\n" );
        unregister_chrdev_region( dev, 1 );
        return -EINVAL;
    }

     if( device_create( cl, NULL, dev, NULL, "sample" ) == NULL )
    {
        printk( KERN_ALERT ">>> ERROR : Device creation failed\n" );
        class_destroy(cl);
        unregister_chrdev_region( dev, 1 );
        return -EINVAL;
    }

    cdev_add(my_cdev,dev,1);    


    return 0;
}
static void buff_cleanup(void){
    int i;
    //-------------------------------------
    printk(KERN_ALERT "Driver is stoping\n");
    cdev_del(my_cdev);
    device_destroy( cl, dev );
    class_destroy( cl );
    unregister_chrdev_region(dev,1);
    if(buff){
        printk(KERN_ALERT "Buffer free \n");
        for(i = 0; i < buff->buffer_size ; i = i + 1){
            if(buff->buffer_content[i].size_data != 0)
                kfree(buff->buffer_content[i].data);
        }
        kfree(buff);
    }
}

MODULE_LICENSE(LICENCE);
MODULE_AUTHOR(AUTEUR);
MODULE_DESCRIPTION(DESCRIPTION);
MODULE_SUPPORTED_DEVICE(DEVICE);

module_init(buff_init);
module_exit(buff_cleanup);

